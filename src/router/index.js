import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  //after login
  {
    path: '/',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/dashboard', 
        name: 'dashboard', 
        meta: {
          requiresAuth: true,
          title:'Dashboard',
          hidetopnav:true
        },
        component: () => import('@/views/dashboard/dashboard.vue')
      },
    ]
  },
  {
    path: '/',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/clients', 
        name: 'clients', 
        meta: {
          requiresAuth: true,
          title:'Dashboard',
          hidetopnav:true
        },
        component: () => import('@/views/clients/clients.vue')
      },
    ]
  },
  {
    path: '/',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/task', 
        name: 'task', 
        meta: {
          requiresAuth: true,
          title:'Task',
          hidetopnav:true
        },
        component: () => import('@/views/task/task.vue')
      },
    ]
  },
  {
    path: '/',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/documents', 
        name: 'documents', 
        meta: {
          requiresAuth: true,
          title:'Client List',
          hidetopnav:true
        },
        component: () => import('@/views/documents/documents.vue')
      },
    ]
  },
  {
    path: '/',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/members', 
        name: 'members', 
        meta: {
          requiresAuth: true,
          title:'Members',
          hidetopnav:true
        },
        component: () => import('@/views/members/members.vue')
      },
      {
        path: '/member-details', 
        name: 'memberdetails', 
        meta: {
          requiresAuth: true,
          title:'Members',
          hidetopnav:true
        },
        component: () => import('@/views/members/member-details.vue')
      },
    ]
  },
  {
    path: '/',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/schedule', 
        name: 'schedule', 
        meta: {
          requiresAuth: true,
          title:'Dashboard',
          hidetopnav:true
        },
        component: () => import('@/views/schedule/schedule.vue')
      },
    ]
  }, 
  {
    path: '/switch-account',
    component: () => import('@/layouts/Main.vue'),
    children: [
      {
        path: '/switch-account', 
        name: 'Switch Account', 
        meta: {
          requiresAuth: true,
          title:'Switch Account',
          hidetopnav:false
        },
        component: () => import('@/components/switchAccount.vue')
      },
    ]
  },
  
]

const router = new VueRouter({
  routes
})

export default router
